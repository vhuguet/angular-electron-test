import { Component } from '@angular/core';
import { interval } from 'rxjs';
import { tap, takeWhile } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  max =1;
  current = 0;

  // Start the timer
  start(): void {
    const myInterval = interval(100);
    myInterval.pipe(
      takeWhile(_ => !this.isFinished),
      tap(i => this.current += 0.1)
    ).subscribe();
  }

  // Finish timer
  finish(): void {
    this.current = this.max;
  }

  // Reset timer
  reset(): void {
    this.current = 0;
  }

  // Getters to prevent NaN errors
  get maxVal() {
    return isNaN(this.max) || this.max < 0.1 ? 0.1 : this.max;
  }

  get currentVal() {
    return isNaN(this.current) || this.current < 0 ? 0 : this.current;
  }

  get isFinished(): boolean {
    return this.currentVal >= this.maxVal;
  }
}
