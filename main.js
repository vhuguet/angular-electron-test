const { app, BrowserWindow } = require('electron');

let win;

function createWindow() {
    // Create the browser window.
    win = new BrowserWindow({
        width: 600,
        height: 600,
        backgroundColor: '#ffffff',
        icon: `file://${__dirname}/dist/angular-electron-test/assets/logo.png`
    });

    win.loadURL(`file://${__dirname}/dist/angular-electron-test/index.html`);

    // Uncomment below to open DevTools.
    // win.webContents.openDevTools();

    // Event when the windows is closed
    win.on('closed', () => {
        win = null;
    });
}

// Create window on electron initialization
app.on('ready', createWindow);

// Quit when all windows are closed
app.on('window-all-closed', () => {
    // On macOS specific close process
    if(process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', () => {
    // macOS specific close process
    if(win === null) {
        createWindow();
    }
});